<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.codesplai.games.*" %>
<% String genero = request.getParameter("genero");
if(genero!=null){
    Genre g = new Genre(genero);
    GenreController.create(g);}

    String id_g =request.getParameter("id");
    if(id_g!=null){
    Genre i = new Genre(id_g);
    GenreController.delete(i);
    }
    String editar_genero =request.getParameter("edit_genre");
    String editar_id =request.getParameter("edit_id");
    
    if(editar_id != null && editar_genero !=null){
    Genre e = new Genre(editar_id,editar_genero);
    GenreController.edit(e);
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Generos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.jsp">Indice <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="genres.jsp">Generos</a>
      <a class="nav-item nav-link" href="publishers.jsp">Editores</a>
      <a class="nav-item nav-link " href="plataforms.jsp" >Plataformas</a>
      
      <a class="nav-item nav-link " href="formgenero.jsp" >Crear Genero</a>
    </div>
  </div>
</nav>



<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>

    </tr>
  </thead>
  <tbody>
  <% for (Genre gen : GenreController.getAll()) { %>
   
    <tr>
      <td><%= gen.id_genre %></td>
      <td><%= gen.genre %></td>
    </tr>
    <% } %>
  </tbody>
</table>


</body>
</html>