
package com.codesplai.games;

public class Platforms {

    public int id_plataforma;
    public String plataforma;

    public Platforms(int id_plataforma, String plataforma) {
        this.id_plataforma = id_plataforma;
        this.plataforma = plataforma;
    }
    public Platforms(String plataforma) {
        this.plataforma = plataforma;
    }

    public Platforms(String id_plataforma_mal, String plataforma) {
        int id_correcto = Integer.parseInt(id_plataforma_mal);

        this.id_plataforma = id_correcto;
        this.plataforma = plataforma;
    }
}
