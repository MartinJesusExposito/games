
package com.codesplai.games;

public class Publishers {

    public int id_editores;
    public String publisher;

    public Publishers(int id_editores, String publisher) {
        this.id_editores = id_editores;
        this.publisher = publisher;
    }

    public Publishers(String publisher) {
        this.publisher = publisher;
    }

    public Publishers(String id_publisher_mal, String publisher) {
        int id_correcto = Integer.parseInt(id_publisher_mal);

        this.id_editores = id_correcto;
        this.publisher = publisher;
    }
}