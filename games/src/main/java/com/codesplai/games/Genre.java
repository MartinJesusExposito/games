
package com.codesplai.games;

public class Genre {

    public int id_genre;
    public String genre;

    public Genre(int id_genre, String genre) {
        this.id_genre = id_genre;
        this.genre = genre;
    }

    public Genre(String genre) {
        this.genre = genre;
    }

    public Genre(String id_genre_mal, String genre) {
        int id_correcto = Integer.parseInt(id_genre_mal);

        this.id_genre = id_correcto;
        this.genre = genre;
    }
}