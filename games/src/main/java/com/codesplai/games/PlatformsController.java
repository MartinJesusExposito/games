package com.codesplai.games;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class PlatformsController {

    // getAll devuelve todos los registros de la tabla
    public static List<Platforms> getAll(){
        List<Platforms> listaPlatformss = new ArrayList<Platforms>();
        String sql = "select * from plataformas";

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Platforms u = new Platforms(
                    rs.getInt("id_plataforma"),
                    rs.getString("plataforma")
                );
                listaPlatformss.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaPlatformss;

    }

    public static void create(Platforms plat) {
        String sql = "INSERT INTO plataformas (plataforma) VALUES (?)";
     
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, plat.plataforma);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public static void delete(Platforms plat) {
        //delete from generos where id_genero=17;
        String sql = "delete from plataformas where (id_plataforma) = (?)";
    //  int eliminar_id= Integer.parseInt(id.genre);
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, plat.plataforma);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void edit(Platforms plat) {
//UPDATE editores SET publisher="sientiendo" where id_editores=584;
String acomodar= Integer.toString(plat.id_plataforma);
        String sql = "Update plataformas SET plataforma=(?) where (id_plataforma) = (?)";
    //  int eliminar_id= Integer.parseInt(id.genre);
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, plat.plataforma);
            
            pstmt.setString(2, acomodar);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }




}