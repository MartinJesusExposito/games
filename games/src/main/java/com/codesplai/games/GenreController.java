package com.codesplai.games;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class GenreController {

    // getAll devuelve todos los registros de la tabla
    public static List<Genre> getAll(){
        List<Genre> listaGenres = new ArrayList<Genre>();
        String sql = "select * from generos";

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Genre u = new Genre(
                    rs.getInt("id_genero"),
                    rs.getString("genre")
                );
                listaGenres.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaGenres;

    }

    public static void create(Genre genero) {
        String sql = "INSERT INTO generos (genre) VALUES (?)";
     
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, genero.genre);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public static void delete(Genre id) {
        //delete from generos where id_genero=17;
        String sql = "delete from generos where (id_genero) = (?)";
    //  int eliminar_id= Integer.parseInt(id.genre);
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, id.genre);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void edit(Genre edit) {
        //UPDATE generos SET genre="no pedro" where id_genero=15;
String acomodar= Integer.toString(edit.id_genre);
        String sql = "Update  generos SET genre=(?) where (id_genero) = (?)";
    //  int eliminar_id= Integer.parseInt(id.genre);
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, edit.genre);
            
            pstmt.setString(2, acomodar);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }




}