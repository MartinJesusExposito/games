<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.codesplai.games.*" %>
<%@page import="javax.swing.JOptionPane"%>
<% 

Popup.infoBox("YOUR INFORMATION HERE", "TITLE BAR MESSAGE");


%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.jsp">Indice <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="genres.jsp">Generos</a>
      <a class="nav-item nav-link" href="publishers.jsp">Editores</a>
      <a class="nav-item nav-link " href="plataforms.jsp" >Plataformas</a>
    </div>
  </div>
</nav>

<div class="container">
<div class="row">
<div class="col-12">
<h1>Opciones disponibles</h1>
</div>
</div>
<div class="row">
<div class="col">

<form action="genres.jsp">
<div class="form-group">
    <label >Añadir Genero</label>
    <input name="genero" type="text" placeholder="nombre del genero" class="form-control"  >
    <small  class="form-text text-muted">Escribe el nombre del genero que deseas añadir</small>
    <button type="submit" class="btn btn-primary">añadir</button>
  </div>
  </form>
</div>
<div class="col">

<form action="genres.jsp">
<div class="form-group">
    <label for="exampleInputEmail1">Editar Genero</label>
    <input name="edit_id" type="text" placeholder="id a editar" class="form-control">
    <small id="emailHelp" class="form-text text-muted">Que genero quieres editar? Escribe su id.</small>
  <input name="edit_genre" type="text" placeholder="nuevo valor">
    <small id="emailHelp" class="form-text text-muted">Que quieres que ponga ahora? </small>
  
  <button type="submit" class="btn btn-primary">Editar</button>
  </div>
  </form>
  </div>
  <div class="col">

<form action="genres.jsp">
<div class="form-group">
    <label for="exampleInputEmail1">Borrar Genero</label>
    <input name="id" type="text" placeholder="id a eliminar" class="form-control">
    <small id="emailHelp" class="form-text text-muted">Seguro que deseas borrarlo?</small>
    <button type="submit" class="btn btn-danger">Eliminar</button>
  
  </div>
  </form>
  </div>
</div>
</div>

<%-- <form action="genres.jsp">
<input name="genero" type="text" placeholder="nombre del genero">
<input type="submit" value="crear">
</form>
<form action="genres.jsp">
<input name="id" type="text" placeholder="id a eliminar">
<input type="submit" value="eliminar">
</form>
<form action="genres.jsp">
<input name="edit_id" type="text" placeholder="id a editar">
<input name="edit_genre" type="text" placeholder="nuevo valor">
<input type="submit" value="editar">
</form>
 --%>

</body>
</html>