package com.codesplai.games;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class PublishersController {

    // getAll devuelve todos los registros de la tabla
    public static List<Publishers> getAll(){
        List<Publishers> listaPublisherss = new ArrayList<Publishers>();
        String sql = "select * from editores";

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Publishers u = new Publishers(
                    rs.getInt("id_editores"),
                    rs.getString("publisher")
                );
                listaPublisherss.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaPublisherss;

    }

    public static void create(Publishers pub) {
        String sql = "INSERT INTO editores (publisher) VALUES (?)";
     
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, pub.publisher);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public static void delete(Publishers id) {
        //delete from generos where id_genero=17;
        String sql = "delete from editores where (id_editores) = (?)";
    //  int eliminar_id= Integer.parseInt(id.genre);
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, id.publisher);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void edit(Publishers edit) {
//UPDATE editores SET publisher="sientiendo" where id_editores=584;
String acomodar= Integer.toString(edit.id_editores);
        String sql = "Update editores SET publisher=(?) where (id_editores) = (?)";
    //  int eliminar_id= Integer.parseInt(id.genre);
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, edit.publisher);
            
            pstmt.setString(2, acomodar);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }



}